﻿using DVGeometry.Basics;
using DVGeometry.Shapes;
using FluentAssertions;
using FluentAssertions.Execution;

namespace DVGeometryTests.Shapes;

[TestFixture]
[TestOf(typeof(Polyline2D))]
public class Polyline2DTest
{
    [Test]
    public void TypeTest()
    {
        // Arrange

        // Act

        // Assert
        typeof(Polyline2D).Should().BeAssignableTo<IShape>();
    }

    [Test]
    public void PropertiesTest([Range(0, 4)] int verticesNumber)
    {
        // Arrange
        var vertices = new List<Vertex2D>();

        if (verticesNumber > 0)
        {
            vertices.Add(new Vertex2D(new CoordinatesFacade2D(1, 2)));
        }

        if (verticesNumber > 1)
        {
            vertices.Add(new Vertex2D(new CoordinatesFacade2D(3, 4)));
        }

        if (verticesNumber > 2)
        {
            vertices.Add(new Vertex2D(new CoordinatesFacade2D(5, 6)));
        }

        if (verticesNumber > 3)
        {
            vertices.Add(new Vertex2D(new CoordinatesFacade2D(5, 6)));
        }

        var polyline = new Polyline2D(vertices.ToArray());

        // Act

        // Assert
        using (new AssertionScope())
        {
            polyline.Id.Should().NotBeEmpty();
            polyline.Vertices.Should().NotBeNull().And.HaveSameCount(vertices).And
                .OnlyContain(x => vertices.Contains(x))
                .And.ContainInConsecutiveOrder(vertices).And.AllSatisfy(x =>
                    x.Edges.Should().HaveCountGreaterThanOrEqualTo(verticesNumber > 1 ? 1 : 0));
        }
    }
}