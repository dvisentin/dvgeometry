﻿using DVGeometry.Basics;
using DVGeometry.Shapes;
using FluentAssertions;
using FluentAssertions.Execution;

namespace DVGeometryTests.Shapes;

[TestFixture]
[TestOf(typeof(Vertex2D))]
public class Vertex2DTest
{
    [Test]
    public void TypeTest()
    {
        // Arrange

        // Act

        // Assert
        typeof(Vertex2D).Should().BeAssignableTo<Point2D>();
    }

    [Test]
    public void PropertiesTest()
    {
        // Arrange
        var coordinates = new CoordinatesFacade2D(1, 2);
        var vertex = new Vertex2D(coordinates);

        // Act

        // Assert
        using (new AssertionScope())
        {
            vertex.Id.Should().NotBeEmpty();
            vertex.Coordinates.Should().BeEquivalentTo(coordinates);
            vertex.Edges.Should().NotBeNull().And.BeEmpty();
        }
    }

    [Test]
    public void ConnectTest()
    {
        // Arrange
        var firstVertex = new Vertex2D(new CoordinatesFacade2D(1, 2));
        var secondVertex = new Vertex2D(new CoordinatesFacade2D(3, 4));

        // Act
        var segment = Vertex2D.Connect(firstVertex, secondVertex);

        // Assert
        using (new AssertionScope())
        {
            segment.Start.Should().BeEquivalentTo(firstVertex);
            segment.End.Should().BeEquivalentTo(secondVertex);

            firstVertex.Edges.Should().OnlyContain(x => x == segment);
            secondVertex.Edges.Should().OnlyContain(x => x == segment);
        }
    }
}