﻿using DVGeometry.Basics;
using DVGeometry.Shapes;
using FluentAssertions;
using FluentAssertions.Execution;

namespace DVGeometryTests.Shapes;

[TestFixture]
[TestOf(typeof(Point2D))]
public class Point2DTest : BaseFixture
{
    [Test]
    public void TypeTest()
    {
        // Arrange

        // Act

        // Assert
        typeof(Point2D).Should().BeAssignableTo<IShape>();
    }

    [Test]
    public void PropertiesTest()
    {
        // Arrange
        var coordinates = new CoordinatesFacade2D(1, 2);
        var point = new Point2D(coordinates);

        // Act

        // Assert
        using (new AssertionScope())
        {
            point.Id.Should().NotBeEmpty();
            point.Coordinates.Should().BeEquivalentTo(coordinates);
        }
    }

    [TestCase(0, 0, 0, 0, 0, 0, true)]
    [TestCase(0, 0, 0, 0, 1, 2, true)]
    [TestCase(0, 0, 1, 2, 1, 2, true)]
    [TestCase(0, 0, 1, 2, 2, 4, true)]
    [TestCase(0, 0, 1, 2, -2, -4, true)]
    [TestCase(0.5, 1, 1, 2, -2, -4, true)]
    [TestCase(0, 0, 1, 2, -2, -3, false)]
    [TestCase(0, 0, 1, 3, -2, -4, false)]
    [TestCase(0, 1, 1, 2, -2, -4, false)]
    public void AreCollinearTest(double x1, double y1, double x2, double y2, double x3, double y3, bool expectedOutcome)
    {
        // Arrange
        var point1 = new Point2D(new CoordinatesFacade2D(x1, y1));
        var point2 = new Point2D(new CoordinatesFacade2D(x2, y2));
        var point3 = new Point2D(new CoordinatesFacade2D(x3, y3));

        // Act
        var outcome = Point2D.AreCollinear(point1, point2, point3);

        // Assert
        outcome.Should().Be(expectedOutcome);
    }

    [TestCase(0, 0, 0, 0, 0, 0, Direction.Counterclockwise, 0)]
    [TestCase(0, 0, 0, 0, 0, 0, Direction.Clockwise, 0)]
    [TestCase(1, 1, 1, 1, 1, 1, Direction.Counterclockwise, 0)]
    [TestCase(1, 1, 1, 1, 1, 1, Direction.Clockwise, 0)]
    [TestCase(0, 0, 0, 0, 1, 1, Direction.Counterclockwise, 0)]
    [TestCase(0, 0, 0, 0, 1, 1, Direction.Clockwise, 0)]
    [TestCase(2, 2, 1, 1, 2, 2, Direction.Counterclockwise, 0)]
    [TestCase(2, 2, 1, 1, 2, 2, Direction.Clockwise, 0)]
    [TestCase(1, 0, 0, 0, 1, 1, Direction.Counterclockwise, 45)]
    [TestCase(1, 0, 0, 0, 1, 1, Direction.Clockwise, 315)]
    [TestCase(1, 0, 0, 0, 0, 1, Direction.Counterclockwise, 90)]
    [TestCase(1, 0, 0, 0, 0, 1, Direction.Clockwise, 270)]
    [TestCase(1, 1, 0, 0, -1, 1, Direction.Counterclockwise, 90)]
    [TestCase(1, 1, 0, 0, -1, 1, Direction.Clockwise, 270)]
    [TestCase(2, 1, 1, 1, 1, 2, Direction.Counterclockwise, 90)]
    [TestCase(2, 1, 1, 1, 1, 2, Direction.Clockwise, 270)]
    [TestCase(1, 0, 0, 0, -1, 1, Direction.Counterclockwise, 135)]
    [TestCase(1, 0, 0, 0, -1, 1, Direction.Clockwise, 225)]
    [TestCase(1, 0, 0, 0, -1, 0, Direction.Counterclockwise, 180)]
    [TestCase(1, 0, 0, 0, -1, 0, Direction.Clockwise, 180)]
    [TestCase(1, 0, 0, 0, -1, -1, Direction.Counterclockwise, 225)]
    [TestCase(1, 0, 0, 0, -1, -1, Direction.Clockwise, 135)]
    [TestCase(1, 0, 0, 0, 0, -1, Direction.Counterclockwise, 270)]
    [TestCase(1, 0, 0, 0, 0, -1, Direction.Clockwise, 90)]
    public void GetAngleTest(double firstX, double firstY, double originX, double originY, double lastX, double lastY,
        Direction direction, double expectedDegrees)
    {
        // Arrange
        var firstPoint = new Point2D(new CoordinatesFacade2D(firstX, firstY));
        var originPoint = new Point2D(new CoordinatesFacade2D(originX, originY));
        var lastPoint = new Point2D(new CoordinatesFacade2D(lastX, lastY));

        // Act
        var angle = Point2D.GetAngle(firstPoint, originPoint, lastPoint, direction);

        // Assert
        angle.Degrees.Value.Should().BeApproximately(expectedDegrees, SmallEpsilon);
    }
}