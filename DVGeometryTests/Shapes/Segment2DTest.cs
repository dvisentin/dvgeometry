﻿using DVGeometry.Basics;
using DVGeometry.Shapes;
using FluentAssertions;
using FluentAssertions.Execution;

namespace DVGeometryTests.Shapes;

[TestFixture]
[TestOf(typeof(Segment2D))]
public class Segment2DTest
{
    [Test]
    public void TypeTest()
    {
        // Arrange

        // Act

        // Assert
        typeof(Segment2D).Should().BeAssignableTo<IShape>();
    }

    [Test]
    public void PropertiesTest()
    {
        // Arrange
        var startNode = new Vertex2D(new CoordinatesFacade2D(1, 2));
        var endNode = new Vertex2D(new CoordinatesFacade2D(3, 4));
        var segment = new Segment2D(startNode, endNode);

        // Act

        // Assert
        using (new AssertionScope())
        {
            segment.Id.Should().NotBeEmpty();
            segment.Start.Should().BeEquivalentTo(startNode);
            segment.End.Should().BeEquivalentTo(endNode);
        }
    }
}