﻿using System.Numerics;
using DVGeometry.Basics;
using FluentAssertions;
using FluentAssertions.Execution;

namespace DVGeometryTests.Basics;

[TestFixture]
[TestOf(typeof(PolarCoordinates2D))]
public class PolarCoordinates2DTest : BaseFixture
{
    [Test]
    public void DoubleConstructorPropertiesTest()
    {
        // Arrange
        var coordinates = new PolarCoordinates2D(10, new Angle((Degrees) 5));

        // Act

        // Assert
        using (new AssertionScope())
        {
            coordinates.Rho.Should().BeApproximately(10, SmallEpsilon);
            coordinates.Theta.Degrees.Value.Should().BeApproximately(5, SmallEpsilon);
        }
    }

    [TestCase(11.18033988f, 0, 0)]
    [TestCase(10, 5, 26.56505117)]
    [TestCase(0, 11.18033988f, 90)]
    [TestCase(-10, 5, 153.43494882)]
    [TestCase(-11.18033988f, 0, 180)]
    [TestCase(-10, -5, 360 - 153.43494882)]
    [TestCase(0, -11.18033988f, 270)]
    [TestCase(10, -5, 360 - 26.56505117)]
    public void VectorConstructorPropertiesTest(float inputX, float inputY, double expectedDegrees)
    {
        // Arrange
        var coordinates = new PolarCoordinates2D(new Vector2(inputX, inputY));

        // Act

        // Assert
        using (new AssertionScope())
        {
            coordinates.Rho.Should().BeApproximately(11.18033988, SmallEpsilon);
            coordinates.Theta.Degrees.Value.Should().BeApproximately(expectedDegrees, SmallEpsilon);
        }
    }

    [TestCase(0, 0, 0, 0)]
    [TestCase(0, 45, 0, 0)]
    [TestCase(5, 0, 5, 0)]
    [TestCase(5, 45, 3.535534f, 3.535534f)]
    [TestCase(5, 90, 0, 5)]
    [TestCase(5, 135, -3.535534f, 3.535534f)]
    [TestCase(5, 180, -5, 0)]
    [TestCase(5, 225, -3.535534f, -3.535534f)]
    [TestCase(5, 270, 0, -5)]
    [TestCase(5, 315, 3.535534f, -3.535534f)]
    [TestCase(5, 360, 5, 0)]
    public void AsVector2Test(double inputRho, double inputThetaDegrees, float expectedX, float expectedY)
    {
        // Arrange
        var coordinates = new PolarCoordinates2D(inputRho, new Angle(new Degrees(inputThetaDegrees)));

        // Act
        var vector = coordinates.AsVector2();

        // Assert
        using (new AssertionScope())
        {
            vector.Should().BeOfType<Vector2>();
            vector.X.Should().BeApproximately(expectedX, (float) SmallEpsilon);
            vector.Y.Should().BeApproximately(expectedY, (float) SmallEpsilon);
        }
    }
}