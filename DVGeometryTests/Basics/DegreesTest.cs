﻿using DVGeometry.Basics;
using FluentAssertions;
using FluentAssertions.Execution;

namespace DVGeometryTests.Basics;

[TestFixture]
[TestOf(typeof(Degrees))]
public class DegreesTest : BaseFixture
{
    [TestCase(-1, 359)]
    [TestCase(-0.001, 359.999)]
    [TestCase(0, 0)]
    [TestCase(0.001, 0.001)]
    [TestCase(1, 1)]
    [TestCase(359, 359)]
    [TestCase(359.999, 359.999)]
    [TestCase(360, 0)]
    [TestCase(360.001, 0.001)]
    [TestCase(361, 1)]
    public void UsingConstructorResultIsAlwaysModulo360Test(double inputValue, double expectedValue)
    {
        // Arrange

        // Act
        var degrees = new Degrees(inputValue);
        double castedDegrees = degrees;

        // Assert
        using (new AssertionScope())
        {
            degrees.Value.Should().BeInRange(0, 360).And.BeApproximately(expectedValue, SmallEpsilon);
            castedDegrees.Should().BeInRange(0, 360).And.BeApproximately(expectedValue, SmallEpsilon);
        }
    }

    [TestCase(-1, 359)]
    [TestCase(-0.001, 359.999)]
    [TestCase(0, 0)]
    [TestCase(0.001, 0.001)]
    [TestCase(1, 1)]
    [TestCase(359, 359)]
    [TestCase(359.999, 359.999)]
    [TestCase(360, 0)]
    [TestCase(360.001, 0.001)]
    [TestCase(361, 1)]
    public void UsingCastingOperatorResultIsAlwaysModulo360Test(double inputValue, double expectedValue)
    {
        // Arrange

        // Act
        Degrees degrees = inputValue;
        double castedDegrees = degrees;

        // Assert
        using (new AssertionScope())
        {
            degrees.Value.Should().BeInRange(0, 360).And.BeApproximately(expectedValue, SmallEpsilon);
            castedDegrees.Should().BeInRange(0, 360).And.BeApproximately(expectedValue, SmallEpsilon);
        }
    }

    [Test]
    public void AsRadiansTest()
    {
        // Arrange
        var degrees = new Degrees(90);

        // Act
        var radians = degrees.AsRadians();

        // Assert
        using (new AssertionScope())
        {
            radians.Should().BeOfType<Radians>();
            radians.Value.Should().BeApproximately(Math.PI / 2, SmallEpsilon);
        }
    }

    [TestCase(90, 45, 135)]
    [TestCase(270, 180, 90)]
    [TestCase(45, -90, 315)]
    public void SumTest(double first, double second, double expectedSum)
    {
        // Arrange
        var fistDegrees = new Degrees(first);
        var secondDegrees = new Degrees(second);

        // Act
        Degrees sum = fistDegrees + secondDegrees;

        // Assert
        sum.Value.Should().BeApproximately(expectedSum, SmallEpsilon);
    }
}