﻿using System.Numerics;
using DVGeometry.Basics;
using FluentAssertions;
using FluentAssertions.Execution;

namespace DVGeometryTests.Basics;

[TestFixture]
[TestOf(typeof(CartesianCoordinates2D))]
public class CartesianCoordinates2DTest : BaseFixture
{
    [Test]
    public void DoubleConstructorPropertiesTest()
    {
        // Arrange
        var coordinates = new CartesianCoordinates2D(10, 5);

        // Act

        // Assert
        using (new AssertionScope())
        {
            coordinates.X.Should().BeApproximately(10, SmallEpsilon);
            coordinates.Y.Should().BeApproximately(5, SmallEpsilon);
        }
    }

    [Test]
    public void VectorConstructorPropertiesTest()
    {
        // Arrange
        var coordinates = new CartesianCoordinates2D(new Vector2(10, 5));

        // Act

        // Assert
        using (new AssertionScope())
        {
            coordinates.X.Should().BeApproximately(10, SmallEpsilon);
            coordinates.Y.Should().BeApproximately(5, SmallEpsilon);
        }
    }

    [Test]
    public void AsVector2Test()
    {
        // Arrange
        var coordinates = new CartesianCoordinates2D(10, 5);

        // Act
        var vector = coordinates.AsVector2();

        // Assert
        using (new AssertionScope())
        {
            vector.Should().BeOfType<Vector2>();
            vector.X.Should().BeApproximately((float) coordinates.X, (float) SmallEpsilon);
            vector.Y.Should().BeApproximately((float) coordinates.Y, (float) SmallEpsilon);
        }
    }
}