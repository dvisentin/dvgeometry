﻿using System.Numerics;
using DVGeometry.Basics;
using FluentAssertions;
using FluentAssertions.Execution;
using Moq;
using Moq.Protected;

namespace DVGeometryTests.Basics;

[TestFixture]
[TestOf(typeof(CoordinatesTransformation2D.Builder))]
public class CoordinateTransformation2DBuilderTest : BaseFixture
{
    [Test]
    public void DefaultBuildNoOpTest()
    {
        // Arrange
        var builder = new CoordinatesTransformation2D.Builder();

        // Act
        var transformation = builder.Build();

        // Assert
        transformation.IsNoOp.Should().BeTrue();
    }

    [Test]
    public void RotationAroundOriginTest()
    {
        // Arrange
        var matrix = Matrix3x2.Identity;

        var transformationMockup = new Mock<CoordinatesTransformation2D> {CallBase = true};
        transformationMockup.Protected().SetupSet<Matrix3x2>("Matrix", ItExpr.IsAny<Matrix3x2>())
            .Callback(x => matrix = x).CallBase();
        transformationMockup.Protected().SetupGet<Matrix3x2>("Matrix").Returns(matrix);

        var builderMock = new Mock<CoordinatesTransformation2D.Builder> {CallBase = true};
        builderMock.Protected().SetupGet<CoordinatesTransformation2D>("CoordinatesTransformation")
            .Returns(transformationMockup.Object);

        var builder = builderMock.Object;

        // Act
        builder.WithRotationAroundOrigin(new Angle(new Degrees(30))).Build();

        // Assert
        matrix.Should().NotBeNull();
        using (new AssertionScope())
        {
            matrix.M11.Should().BeApproximately(float.Sqrt(3) / 2, (float) SmallEpsilon);
            matrix.M12.Should().BeApproximately(1.0f / 2, (float) SmallEpsilon);
            matrix.M21.Should().BeApproximately(-1.0f / 2, (float) SmallEpsilon);
            matrix.M22.Should().BeApproximately(float.Sqrt(3) / 2, (float) SmallEpsilon);
            matrix.M31.Should().BeApproximately(0, (float) SmallEpsilon);
            matrix.M32.Should().BeApproximately(0, (float) SmallEpsilon);
            matrix.Translation.X.Should().BeApproximately(0, (float) SmallEpsilon);
            matrix.Translation.Y.Should().BeApproximately(0, (float) SmallEpsilon);
        }
    }

    [Test]
    public void RotationAroundCoordinatesTest()
    {
        // Arrange
        var matrix = Matrix3x2.Identity;

        var transformationMockup = new Mock<CoordinatesTransformation2D> {CallBase = true};
        transformationMockup.Protected().SetupSet<Matrix3x2>("Matrix", ItExpr.IsAny<Matrix3x2>())
            .Callback(x => matrix = x).CallBase();
        transformationMockup.Protected().SetupGet<Matrix3x2>("Matrix").Returns(matrix);

        var builderMock = new Mock<CoordinatesTransformation2D.Builder> {CallBase = true};
        builderMock.Protected().SetupGet<CoordinatesTransformation2D>("CoordinatesTransformation")
            .Returns(transformationMockup.Object);

        var builder = builderMock.Object;

        // Act
        builder.WithRotationAroundCoordinates(new Angle(new Degrees(30)), new CoordinatesFacade2D(1, 2)).Build();

        // Assert
        matrix.Should().NotBeNull();
        using (new AssertionScope())
        {
            matrix.M11.Should().BeApproximately(float.Sqrt(3) / 2, (float) SmallEpsilon);
            matrix.M12.Should().BeApproximately(1.0f / 2, (float) SmallEpsilon);
            matrix.M21.Should().BeApproximately(-1.0f / 2, (float) SmallEpsilon);
            matrix.M22.Should().BeApproximately(float.Sqrt(3) / 2, (float) SmallEpsilon);
            matrix.M31.Should().BeApproximately(1 - 1 * float.Sqrt(3) / 2 + 2 * 1.0f / 2, (float) SmallEpsilon);
            matrix.M32.Should().BeApproximately(2 - 1 * 1.0f / 2 - 2 * float.Sqrt(3) / 2, (float) SmallEpsilon);
            matrix.Translation.X.Should()
                .BeApproximately(1 - 1 * float.Sqrt(3) / 2 + 2 * 1.0f / 2, (float) SmallEpsilon);
            matrix.Translation.Y.Should()
                .BeApproximately(2 - 1 * 1.0f / 2 - 2 * float.Sqrt(3) / 2, (float) SmallEpsilon);
        }
    }

    [Test]
    public void CartesianTranslationTest()
    {
        // Arrange
        var matrix = Matrix3x2.Identity;

        var transformationMockup = new Mock<CoordinatesTransformation2D> {CallBase = true};
        transformationMockup.Protected().SetupSet<Matrix3x2>("Matrix", ItExpr.IsAny<Matrix3x2>())
            .Callback(x => matrix = x).CallBase();
        transformationMockup.Protected().SetupGet<Matrix3x2>("Matrix").Returns(matrix);

        var builderMock = new Mock<CoordinatesTransformation2D.Builder> {CallBase = true};
        builderMock.Protected().SetupGet<CoordinatesTransformation2D>("CoordinatesTransformation")
            .Returns(transformationMockup.Object);

        var builder = builderMock.Object;

        // Act
        builder.WithCartesianTranslation(1, 2).Build();

        // Assert
        matrix.Should().NotBeNull();
        using (new AssertionScope())
        {
            matrix.M11.Should().BeApproximately(1, (float) SmallEpsilon);
            matrix.M12.Should().BeApproximately(0, (float) SmallEpsilon);
            matrix.M21.Should().BeApproximately(0, (float) SmallEpsilon);
            matrix.M22.Should().BeApproximately(1, (float) SmallEpsilon);
            matrix.M31.Should().BeApproximately(1, (float) SmallEpsilon);
            matrix.M32.Should().BeApproximately(2, (float) SmallEpsilon);
            matrix.Translation.X.Should().BeApproximately(1, (float) SmallEpsilon);
            matrix.Translation.Y.Should().BeApproximately(2, (float) SmallEpsilon);
        }
    }

    [Test]
    public void PolarTranslationTest()
    {
        // Arrange
        var matrix = Matrix3x2.Identity;

        var transformationMockup = new Mock<CoordinatesTransformation2D> {CallBase = true};
        transformationMockup.Protected().SetupSet<Matrix3x2>("Matrix", ItExpr.IsAny<Matrix3x2>())
            .Callback(x => matrix = x).CallBase();
        transformationMockup.Protected().SetupGet<Matrix3x2>("Matrix").Returns(matrix);

        var builderMock = new Mock<CoordinatesTransformation2D.Builder> {CallBase = true};
        builderMock.Protected().SetupGet<CoordinatesTransformation2D>("CoordinatesTransformation")
            .Returns(transformationMockup.Object);

        var builder = builderMock.Object;

        // Act
        builder.WithPolarTranslation(10, new Angle(new Degrees(30))).Build();

        // Assert
        matrix.Should().NotBeNull();
        using (new AssertionScope())
        {
            matrix.M11.Should().BeApproximately(1, (float) SmallEpsilon);
            matrix.M12.Should().BeApproximately(0, (float) SmallEpsilon);
            matrix.M21.Should().BeApproximately(0, (float) SmallEpsilon);
            matrix.M22.Should().BeApproximately(1, (float) SmallEpsilon);
            matrix.M31.Should().BeApproximately(8.6602545f, (float) SmallEpsilon);
            matrix.M32.Should().BeApproximately(5, (float) SmallEpsilon);
            matrix.Translation.X.Should().BeApproximately(8.6602545f, (float) SmallEpsilon);
            matrix.Translation.Y.Should().BeApproximately(5, (float) SmallEpsilon);
        }
    }

    [Test]
    public void CoordinatesTranslationTest()
    {
        // Arrange
        var matrix = Matrix3x2.Identity;

        var transformationMockup = new Mock<CoordinatesTransformation2D> {CallBase = true};
        transformationMockup.Protected().SetupSet<Matrix3x2>("Matrix", ItExpr.IsAny<Matrix3x2>())
            .Callback(x => matrix = x).CallBase();
        transformationMockup.Protected().SetupGet<Matrix3x2>("Matrix").Returns(matrix);

        var builderMock = new Mock<CoordinatesTransformation2D.Builder> {CallBase = true};
        builderMock.Protected().SetupGet<CoordinatesTransformation2D>("CoordinatesTransformation")
            .Returns(transformationMockup.Object);

        var builder = builderMock.Object;

        // Act
        builder.WithCoordinatesTranslation(new CoordinatesFacade2D(3, 2.5), new CoordinatesFacade2D(4, 4.5)).Build();

        // Assert
        matrix.Should().NotBeNull();
        using (new AssertionScope())
        {
            matrix.M11.Should().BeApproximately(1, (float) SmallEpsilon);
            matrix.M12.Should().BeApproximately(0, (float) SmallEpsilon);
            matrix.M21.Should().BeApproximately(0, (float) SmallEpsilon);
            matrix.M22.Should().BeApproximately(1, (float) SmallEpsilon);
            matrix.M31.Should().BeApproximately(1, (float) SmallEpsilon);
            matrix.M32.Should().BeApproximately(2, (float) SmallEpsilon);
            matrix.Translation.X.Should().BeApproximately(1, (float) SmallEpsilon);
            matrix.Translation.Y.Should().BeApproximately(2, (float) SmallEpsilon);
        }
    }

    [TestCase(-1.5)]
    [TestCase(-1)]
    [TestCase(-0.5)]
    [TestCase(0)]
    [TestCase(0.5)]
    [TestCase(1)]
    [TestCase(1.5)]
    public void UniformScalingTest(double factor)
    {
        // Arrange
        var matrix = Matrix3x2.Identity;

        var transformationMockup = new Mock<CoordinatesTransformation2D> {CallBase = true};
        transformationMockup.Protected().SetupSet<Matrix3x2>("Matrix", ItExpr.IsAny<Matrix3x2>())
            .Callback(x => matrix = x).CallBase();
        transformationMockup.Protected().SetupGet<Matrix3x2>("Matrix").Returns(matrix);

        var builderMock = new Mock<CoordinatesTransformation2D.Builder> {CallBase = true};
        builderMock.Protected().SetupGet<CoordinatesTransformation2D>("CoordinatesTransformation")
            .Returns(transformationMockup.Object);

        var builder = builderMock.Object;

        // Act
        builder.WithUniformScaling(factor).Build();

        // Assert
        matrix.Should().NotBeNull();
        using (new AssertionScope())
        {
            matrix.M11.Should().BeApproximately((float) factor, (float) SmallEpsilon);
            matrix.M12.Should().BeApproximately(0, (float) SmallEpsilon);
            matrix.M21.Should().BeApproximately(0, (float) SmallEpsilon);
            matrix.M22.Should().BeApproximately((float) factor, (float) SmallEpsilon);
            matrix.M31.Should().BeApproximately(0, (float) SmallEpsilon);
            matrix.M32.Should().BeApproximately(0, (float) SmallEpsilon);
            matrix.Translation.X.Should().BeApproximately(0, (float) SmallEpsilon);
            matrix.Translation.Y.Should().BeApproximately(0, (float) SmallEpsilon);
        }
    }
}