﻿using DVGeometry.Basics;
using FluentAssertions;
using FluentAssertions.Execution;

namespace DVGeometryTests.Basics;

[TestFixture]
[TestOf(typeof(Angle))]
public class AngleTest : BaseFixture
{
    [Test]
    public void DegreesConstructorPropertiesTest()
    {
        // Arrange
        var angle = new Angle(new Degrees(90));

        // Act

        // Assert
        using (new AssertionScope())
        {
            angle.Degrees.Value.Should().BeApproximately(90, SmallEpsilon);
            angle.Radians.Value.Should().BeApproximately(Math.PI / 2, SmallEpsilon);
        }
    }

    [Test]
    public void RadiansConstructorPropertiesTest()
    {
        // Arrange
        var angle = new Angle(new Radians(Math.PI / 2));

        // Act

        // Assert
        using (new AssertionScope())
        {
            angle.Degrees.Value.Should().BeApproximately(90, SmallEpsilon);
            angle.Radians.Value.Should().BeApproximately(Math.PI / 2, SmallEpsilon);
        }
    }
}