﻿using System.Numerics;
using DVGeometry.Basics;
using FluentAssertions;
using Moq;
using Moq.Protected;

namespace DVGeometryTests.Basics;

[TestFixture]
[TestOf(typeof(CoordinatesTransformation2D))]
public class CoordinatesTransformation2DTest : BaseFixture
{
    [Test]
    public void IsNoOpTest([Values(true, false)] bool matrixIsIdentity)
    {
        // Arrange
        var transformationMock = new Mock<CoordinatesTransformation2D>();
        transformationMock.Protected().SetupGet<Matrix3x2>("Matrix")
            .Returns(matrixIsIdentity ? Matrix3x2.Identity : Matrix3x2.Negate(Matrix3x2.Identity));

        var transformation = transformationMock.Object;

        // Act

        // Assert
        transformation.IsNoOp.Should().Be(matrixIsIdentity);
    }
}