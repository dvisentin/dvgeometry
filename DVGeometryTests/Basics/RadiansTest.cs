﻿using DVGeometry.Basics;
using FluentAssertions;
using FluentAssertions.Execution;

namespace DVGeometryTests.Basics;

[TestFixture]
[TestOf(typeof(Radians))]
public class RadiansTest : BaseFixture
{
    [TestCase(-1, 2 * Math.PI - 1)]
    [TestCase(-0.001, 2 * Math.PI - 0.001)]
    [TestCase(0, 0)]
    [TestCase(0.001, 0.001)]
    [TestCase(1, 1)]
    [TestCase(Math.PI, Math.PI)]
    [TestCase(2 * Math.PI - 1, 2 * Math.PI - 1)]
    [TestCase(2 * Math.PI - 0.001, 2 * Math.PI - 0.001)]
    [TestCase(2 * Math.PI, 0)]
    [TestCase(2 * Math.PI + 0.001, 0.001)]
    [TestCase(2 * Math.PI + 1, 1)]
    public void UsingConstructorResultIsAlwaysModulo2PiTest(double inputValue, double expectedValue)
    {
        // Arrange

        // Act
        var radians = new Radians(inputValue);
        double castedRadians = radians;

        // Assert
        using (new AssertionScope())
        {
            radians.Value.Should().BeInRange(0, 2 * Math.PI).And.BeApproximately(expectedValue, SmallEpsilon);
            castedRadians.Should().BeInRange(0, 2 * Math.PI).And.BeApproximately(expectedValue, SmallEpsilon);
        }
    }

    [TestCase(-1, 2 * Math.PI - 1)]
    [TestCase(-0.001, 2 * Math.PI - 0.001)]
    [TestCase(0, 0)]
    [TestCase(0.001, 0.001)]
    [TestCase(1, 1)]
    [TestCase(Math.PI, Math.PI)]
    [TestCase(2 * Math.PI - 1, 2 * Math.PI - 1)]
    [TestCase(2 * Math.PI - 0.001, 2 * Math.PI - 0.001)]
    [TestCase(2 * Math.PI, 0)]
    [TestCase(2 * Math.PI + 0.001, 0.001)]
    [TestCase(2 * Math.PI + 1, 1)]
    public void UsingCastingOperatorResultIsAlwaysModulo2PiTest(double inputValue, double expectedValue)
    {
        // Arrange

        // Act
        Radians radians = inputValue;
        double castedRadians = radians;

        // Assert
        using (new AssertionScope())
        {
            radians.Value.Should().BeInRange(0, 2 * Math.PI).And.BeApproximately(expectedValue, SmallEpsilon);
            castedRadians.Should().BeInRange(0, 2 * Math.PI).And.BeApproximately(expectedValue, SmallEpsilon);
        }
    }

    [Test]
    public void AsRadiansTest()
    {
        // Arrange
        var radians = new Radians(Math.PI / 2);

        // Act
        var degrees = radians.AsDegrees();

        // Assert
        using (new AssertionScope())
        {
            degrees.Should().BeOfType<Degrees>();
            degrees.Value.Should().BeApproximately(90, SmallEpsilon);
        }
    }

    [TestCase(Math.PI / 2, Math.PI / 4, Math.PI * 3 / 4)]
    [TestCase(Math.PI * 3 / 2, Math.PI, Math.PI / 2)]
    [TestCase(Math.PI / 4, -Math.PI / 2, Math.PI + Math.PI * 3 / 4)]
    public void SumTest(double first, double second, double expectedSum)
    {
        // Arrange
        var fistRadians = new Radians(first);
        var secondRadians = new Radians(second);

        // Act
        Radians sum = fistRadians + secondRadians;

        // Assert
        sum.Value.Should().BeApproximately(expectedSum, SmallEpsilon);
    }
}