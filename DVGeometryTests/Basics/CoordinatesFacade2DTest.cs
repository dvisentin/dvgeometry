﻿using DVGeometry.Basics;
using FluentAssertions;
using FluentAssertions.Execution;

namespace DVGeometryTests.Basics;

[TestFixture]
[TestOf(typeof(CoordinatesFacade2D))]
public class CoordinatesFacade2DTest : BaseFixture
{
    [Test]
    public void PropertiesTest()
    {
        // Arrange
        var coordinates = new CoordinatesFacade2D(10, 5);

        // Act

        // Assert
        using (new AssertionScope())
        {
            coordinates.CartesianCoordinates.X.Should().BeApproximately(10, SmallEpsilon);
            coordinates.CartesianCoordinates.Y.Should().BeApproximately(5, SmallEpsilon);

            coordinates.PolarCoordinates.Rho.Should().BeApproximately(11.18033988, SmallEpsilon);
            coordinates.PolarCoordinates.Theta.Degrees.Value.Should().BeApproximately(26.56505117, SmallEpsilon);
        }
    }
}