﻿using DVGeometry.Basics;

namespace DVGeometry.Shapes;

public class Vertex2D : Point2D
{
    public IEnumerable<Segment2D> Edges { get; private set; }

    public Vertex2D(CoordinatesFacade2D coordinates) : base(coordinates)
    {
        Edges = Enumerable.Empty<Segment2D>();
    }

    public static Segment2D Connect(Vertex2D first, Vertex2D second)
    {
        var segment = new Segment2D(first, second);

        first.Edges = first.Edges.Append(segment);
        second.Edges = second.Edges.Append(segment);

        return segment;
    }
}