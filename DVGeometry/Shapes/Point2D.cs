﻿using System.Numerics;
using DVGeometry.Basics;

namespace DVGeometry.Shapes;

public class Point2D : IShape
{
    public Guid Id { get; }

    public CoordinatesFacade2D Coordinates { get; }

    public Point2D(CoordinatesFacade2D coordinates)
    {
        Id = Guid.NewGuid();
        Coordinates = coordinates;
    }

    public static bool AreCollinear(Point2D point1, Point2D point2, Point2D point3)
    {
        var point1Coordinates = point1.Coordinates.CartesianCoordinates;
        var point2Coordinates = point2.Coordinates.CartesianCoordinates;
        var point3Coordinates = point3.Coordinates.CartesianCoordinates;

        double determinant = 0;
        determinant += point1Coordinates.X * point2Coordinates.Y;
        determinant += point1Coordinates.Y * point3Coordinates.X;
        determinant += point2Coordinates.X * point3Coordinates.Y;
        determinant -= point3Coordinates.X * point2Coordinates.Y;
        determinant -= point3Coordinates.Y * point1Coordinates.X;
        determinant -= point2Coordinates.X * point1Coordinates.Y;

        return determinant is > -0.000001 and < 0.000001;
    }

    public static Angle GetAngle(Point2D first, Point2D origin, Point2D last, Direction direction)
    {
        var vector1 = direction is Direction.Counterclockwise ? CreateVector(first) : CreateVector(last);
        var vector2 = direction is Direction.Counterclockwise ? CreateVector(last) : CreateVector(first);

        var determinant = vector1.X * vector2.Y - vector1.Y * vector2.X;
        var dotProduct = vector1.X * vector2.X + vector1.Y * vector2.Y;

        var radians = Math.Atan2(determinant, dotProduct);

        return new Angle(new Radians(radians));

        Vector2 CreateVector(Point2D point)
        {
            return new Vector2(
                (float) (point.Coordinates.CartesianCoordinates.X - origin.Coordinates.CartesianCoordinates.X),
                (float) (point.Coordinates.CartesianCoordinates.Y - origin.Coordinates.CartesianCoordinates.Y));
        }
    }
}