﻿namespace DVGeometry.Shapes;

public interface IShape
{
    public Guid Id { get; }
}