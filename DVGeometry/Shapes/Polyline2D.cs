﻿namespace DVGeometry.Shapes;

public class Polyline2D : IShape
{
    public Guid Id { get; }

    public IEnumerable<Vertex2D> Vertices { get; }

    public Polyline2D(params Vertex2D[] vertices)
    {
        Id = Guid.NewGuid();

        for (var i = 0; i < vertices.Length - 1; i++)
        {
            Vertex2D.Connect(vertices[i], vertices[i + 1]);
        }

        Vertices = (Vertex2D[]) vertices.Clone();
    }
}