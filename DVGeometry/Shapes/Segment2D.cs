﻿namespace DVGeometry.Shapes;

public class Segment2D : IShape
{
    public Guid Id { get; }

    public Vertex2D Start { get; }

    public Vertex2D End { get; }

    public Segment2D(Vertex2D start, Vertex2D end)
    {
        Id = Guid.NewGuid();
        Start = start;
        End = end;
    }
}