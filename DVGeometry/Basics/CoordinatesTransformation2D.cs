﻿using System.Numerics;

namespace DVGeometry.Basics;

// ReSharper disable once ClassWithVirtualMembersNeverInherited.Global
public record CoordinatesTransformation2D
{
    private Matrix3x2 _matrix;

    // ReSharper disable once UnusedMember.Global
    protected CoordinatesTransformation2D()
    {
    }

    private CoordinatesTransformation2D(Matrix3x2 matrix)
    {
        _matrix = matrix;
    }

    protected virtual Matrix3x2 Matrix
    {
        get => _matrix;
        set => _matrix = value;
    }

    public bool IsNoOp => Matrix.IsIdentity;

    // ReSharper disable once ClassWithVirtualMembersNeverInherited.Global
    public class Builder
    {
        // ReSharper disable once VirtualMemberNeverOverridden.Global
        protected virtual CoordinatesTransformation2D CoordinatesTransformation { get; } = new(Matrix3x2.Identity);

        public CoordinatesTransformation2D Build()
        {
            return CoordinatesTransformation;
        }

        public Builder WithRotationAroundOrigin(Angle angle)
        {
            return WithRotationAroundCoordinates(angle, new CoordinatesFacade2D(0, 0));
        }

        public Builder WithRotationAroundCoordinates(Angle angle, CoordinatesFacade2D coordinates)
        {
            var rotationMatrix =
                Matrix3x2.CreateRotation((float) angle.Radians, coordinates.CartesianCoordinates.AsVector2());
            CoordinatesTransformation.Matrix = Matrix3x2.Multiply(CoordinatesTransformation.Matrix, rotationMatrix);
            return this;
        }

        public Builder WithCartesianTranslation(double x, double y)
        {
            var translationMatrix = Matrix3x2.CreateTranslation((float) x, (float) y);
            CoordinatesTransformation.Matrix = Matrix3x2.Multiply(CoordinatesTransformation.Matrix, translationMatrix);
            return this;
        }

        public Builder WithPolarTranslation(double rho, Angle angle)
        {
            var vector = new PolarCoordinates2D(rho, angle).AsVector2();
            return WithCartesianTranslation(vector.X, vector.Y);
        }

        public Builder WithCoordinatesTranslation(CoordinatesFacade2D from, CoordinatesFacade2D to)
        {
            var fromCartesianCoordinates = from.CartesianCoordinates;
            var toCartesianCoordinates = to.CartesianCoordinates;
            return WithCartesianTranslation(toCartesianCoordinates.X - fromCartesianCoordinates.X,
                toCartesianCoordinates.Y - fromCartesianCoordinates.Y);
        }

        public Builder WithUniformScaling(double scalingFactor)
        {
            var scalingMatrix = Matrix3x2.CreateScale((float) scalingFactor, (float) scalingFactor);
            CoordinatesTransformation.Matrix = Matrix3x2.Multiply(CoordinatesTransformation.Matrix, scalingMatrix);
            return this;
        }
    }
}