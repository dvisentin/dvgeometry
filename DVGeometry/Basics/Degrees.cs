﻿namespace DVGeometry.Basics;

public readonly struct Degrees
{
    private readonly double _value;

    public double Value
    {
        get => _value;
        private init => _value = Utils.Modulo(value, 360);
    }

    public Degrees(double value)
    {
        Value = value;
    }

    public static implicit operator Degrees(double value)
    {
        return new Degrees(value);
    }

    public static implicit operator double(Degrees degrees)
    {
        return degrees.Value;
    }

    public Radians AsRadians()
    {
        return new Radians(this * Math.PI / 180);
    }
}