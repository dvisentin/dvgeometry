﻿namespace DVGeometry.Basics;

public enum Direction
{
    Counterclockwise,
    Clockwise
}