﻿using System.Numerics;

namespace DVGeometry.Basics;

public readonly struct PolarCoordinates2D
{
    public double Rho { get; }

    public Angle Theta { get; }

    public PolarCoordinates2D(double rho, Angle theta)
    {
        Rho = rho;
        Theta = theta;
    }

    internal PolarCoordinates2D(Vector2 vector)
    {
        Rho = Math.Sqrt(Math.Pow(vector.X, 2) + Math.Pow(vector.Y, 2));
        var atan = Math.Atan(vector.Y / vector.X);
        if (vector.X < 0)
        {
            atan += Math.PI;
        }

        Theta = new Angle(new Radians(atan));
    }

    internal Vector2 AsVector2()
    {
        var x = (float) (Rho * Math.Cos(Theta.Radians.Value));
        var y = (float) (Rho * Math.Sin(Theta.Radians.Value));

        return new Vector2(x, y);
    }
}