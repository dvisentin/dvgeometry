﻿using System.Numerics;

namespace DVGeometry.Basics;

public readonly struct CartesianCoordinates2D
{
    public double X { get; }

    public double Y { get; }

    public CartesianCoordinates2D(double x, double y)
    {
        X = x;
        Y = y;
    }

    internal CartesianCoordinates2D(Vector2 vector)
    {
        X = vector.X;
        Y = vector.Y;
    }

    internal Vector2 AsVector2()
    {
        return new Vector2((float) X, (float) Y);
    }
}