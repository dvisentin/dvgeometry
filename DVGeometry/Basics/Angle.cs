﻿namespace DVGeometry.Basics;

public readonly struct Angle
{
    public Degrees Degrees { get; }

    public Radians Radians { get; }

    public Angle(Degrees degrees)
    {
        Degrees = degrees;
        Radians = Degrees.AsRadians();
    }

    public Angle(Radians radians)
    {
        Radians = radians;
        Degrees = Radians.AsDegrees();
    }
}