﻿using System.Numerics;

namespace DVGeometry.Basics;

public readonly struct CoordinatesFacade2D
{
    private readonly Vector2 _vector;

    public CartesianCoordinates2D CartesianCoordinates => new(_vector);

    public PolarCoordinates2D PolarCoordinates => new(_vector);

    public CoordinatesFacade2D(double x, double y)
    {
        _vector = new Vector2((float) x, (float) y);
    }
}