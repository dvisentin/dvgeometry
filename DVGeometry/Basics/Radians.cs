﻿namespace DVGeometry.Basics;

public readonly struct Radians
{
    private readonly double _value;

    public double Value
    {
        get => _value;
        private init => _value = Utils.Modulo(value, 2 * Math.PI);
    }

    public Radians(double value)
    {
        Value = value;
    }

    public static implicit operator Radians(double value)
    {
        return new Radians(value);
    }

    public static implicit operator double(Radians radians)
    {
        return radians.Value;
    }

    public Degrees AsDegrees()
    {
        return new Degrees(this * 180 / Math.PI);
    }
}