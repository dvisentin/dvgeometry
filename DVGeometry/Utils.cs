﻿using System.Numerics;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("DVGeometryTests")]

namespace DVGeometry;

public class Utils
{
    public static TValue Modulo<TValue>(TValue value, TValue modulo)
        where TValue : INumber<TValue>, IAdditionOperators<TValue, TValue, TValue>
    {
        return (value + modulo) % modulo;
    }
}